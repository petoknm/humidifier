#!/bin/bash
set -euo pipefail

python -m venv .venv
source .venv/bin/activate
pip install msgpack

python << EOF
import msgpack
import glob
from pathlib import Path

def get_contents():
    for path in glob.glob('public/*'):
        with open(path, 'rb') as infile:
            data = infile.read()
        path = Path(path)
        path = Path("/", *path.parts[1:])
        yield str(path), data

with open('public.msgpack', 'wb') as outfile:
    packed = msgpack.packb({ path: data for path, data in get_contents() })
    outfile.write(packed)

EOF