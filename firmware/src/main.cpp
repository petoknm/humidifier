#include <memory>

// WIFI
#include <WiFi.h>

// ESP32
// #include <ESPmDNS.h>
#include <esp32-hal-log.h>
#include <esp_task_wdt.h>
#include <esp_wifi.h>

#include "channel.h"
#include "piezo_task.h"
#include "sensor_task.h"
#include "server_task.h"

void connect_smartconfig() {
    WiFi.persistent(true);
    WiFi.mode(WIFI_AP_STA);
    if (!WiFi.beginSmartConfig()) {
        log_i("Failed to start smartconfig");
        while (true) delay(1000);
    }
    log_i("Waiting for SmartConfig");
    while (!WiFi.smartConfigDone()) delay(100);
    log_i("SmartConfig received");
    log_i("Waiting for WiFi");
    while (WiFi.status() != WL_CONNECTED) delay(100);
    log_i("WiFi connected");
    auto ip = WiFi.localIP().toString();
    log_i("IP Address: %s", ip.c_str());
}

bool connect_default(int timeout_ms = 20000) {
    log_i("Trying to connect to WiFi");
    WiFi.mode(WIFI_STA);
    assert(esp_wifi_set_ps(WIFI_PS_NONE) == ESP_OK);
    WiFi.begin();
    while (WiFi.status() != WL_CONNECTED) {
        delay(100);
        timeout_ms -= 100;
        if (timeout_ms <= 0) return false;
    }
    log_i("WiFi connected.");
    auto ip = WiFi.localIP().toString();
    log_i("IP Address: %s", ip.c_str());
    return true;
}

void setup_wifi() {
    if (connect_default()) return;
    connect_smartconfig();
}

// void setup_mdns() {
//     if (!MDNS.begin("humi")) {
//         log_i("Error setting up mDNS responder!");
//         while (true) delay(1000);
//     }
//     log_i("mDNS responder started");
//     MDNS.addService("https", "tcp", 443);
// }

void setup() {
    esp_task_wdt_init(25, true);
    setup_wifi();
    // setup_mdns();

    auto [tx1, rx1] = make_channel<Measurement>(8);
    auto [tx2, rx2] = make_channel<Measurement>(8);
    auto [tx3, rx3] = make_channel<PiezoState>(8);

    SensorTask sensor { tx1 };
    PiezoTask piezo { rx1, tx3, 16, 1 };
    ServerTask server { rx2, rx3, piezo };

    if (!sensor.start()) {
        log_i("Failed to start sensor task");
        while (true) delay(1000);
    }

    if (!piezo.start()) {
        log_i("Failed to start piezo task");
        while (true) delay(1000);
    }

    if (!server.start()) {
        log_i("Failed to start server task");
        while (true) delay(1000);
    }

    // To keep tasks alive we cannot exit this scope
    while (true) loop();
}

void loop() { delay(5000); }
