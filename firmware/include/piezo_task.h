#pragma once

#include "channel.h"
#include "measurement.h"
#include "multi_receiver.h"
#include <Arduino.h>
#include <esp32-hal-log.h>
#include <optional>
#include <variant>

enum class PiezoMode : uint8_t {
    Auto,
    On,
    Off,
};

struct PiezoState {
    PiezoMode mode;
    float humi_lo, humi_hi;
    float freq;
    bool running;
};

class PiezoTask {
    OwningReceiver<Measurement> measurement_rx;
    OwningSender<PiezoState> state_tx;
    int pin;
    int channel;

    struct SetThreshold {
        float lo, hi;
    };
    struct SetFrequency {
        float freq;
    };
    using Cmd = std::variant<SetThreshold, SetFrequency, PiezoMode>;

    Queue<Cmd> ctrl_queue { 8 };

    enum class ThresholdState {
        TurnOn,
        DoNothing,
        TurnOff,
    };

    static ThresholdState calculateThresholdState(float humi, float humi_lo, float humi_hi) {
        if (humi <= humi_lo) return ThresholdState::TurnOn;
        if (humi >= humi_hi) return ThresholdState::TurnOff;
        return ThresholdState::DoNothing;
    }

    static constexpr int priority = 10;

public:
    PiezoTask(OwningReceiver<Measurement> measurement_rx, OwningSender<PiezoState> state_tx, int pin, int channel)
        : measurement_rx(measurement_rx)
        , state_tx(state_tx)
        , pin(pin)
        , channel(channel) { }

    [[nodiscard]] bool start() {
        auto res = xTaskCreatePinnedToCore(
            [](void* arg) { reinterpret_cast<PiezoTask*>(arg)->task(); },
            "piezod", 1024, this, priority, nullptr, 1);
        return res == pdPASS;
    }

    void setThreshold(float lo, float hi) {
        assert(ctrl_queue.send(SetThreshold { .lo = lo, .hi = hi }));
    }

    void setFrequency(float freq) {
        assert(ctrl_queue.send(SetFrequency { .freq = freq }));
    }

    void setMode(PiezoMode mode) {
        assert(ctrl_queue.send(mode));
    }

private:
    void task() {
        float last_humi { 50 };
        PiezoState state {
            .mode = PiezoMode::Auto,
            .humi_lo = 45,
            .humi_hi = 55,
            .freq = 108000,
            .running = false,
        };

        ledcAttachPin(pin, channel);
        state.freq = ledcSetup(channel, state.freq, 1);
        ledcWrite(channel, 0);

        auto rx = make_multi_receiver(16, ctrl_queue, measurement_rx);
        decltype(rx)::Item item;

        auto onMeasurement = [&](Measurement m) {
            last_humi = m.humi;

            auto thresholdState = calculateThresholdState(last_humi, state.humi_lo, state.humi_hi);
            if (thresholdState == ThresholdState::TurnOn) state.running = true;
            if (thresholdState == ThresholdState::TurnOff) state.running = false;
            ledcWrite(channel, state.running ? 1 : 0);

            assert(state_tx.send(state));
        };
        auto onSetThreshold = [&](SetThreshold cmd) {
            state.humi_lo = cmd.lo;
            state.humi_hi = cmd.hi;

            auto thresholdState = calculateThresholdState(last_humi, state.humi_lo, state.humi_hi);
            if (thresholdState == ThresholdState::TurnOff) state.running = false;
            if (thresholdState == ThresholdState::TurnOn) state.running = true;
            ledcWrite(channel, state.running ? 1 : 0);

            assert(state_tx.send(state));
        };
        auto onSetFreq = [&](SetFrequency cmd) {
            state.freq = ledcSetup(channel, cmd.freq, 1);
            ledcWrite(channel, state.running ? 1 : 0);
            assert(state_tx.send(state));
        };
        auto onSetMode = [&](PiezoMode cmd) {
            state.mode = cmd;
            if (cmd == PiezoMode::Auto) {
                auto thresholdState = calculateThresholdState(last_humi, state.humi_lo, state.humi_hi);
                if (thresholdState == ThresholdState::TurnOff) state.running = false;
                if (thresholdState == ThresholdState::TurnOn) state.running = true;
            }
            if (cmd == PiezoMode::On)
                state.running = true;
            if (cmd == PiezoMode::Off)
                state.running = false;
            ledcWrite(channel, state.running ? 1 : 0);
            assert(state_tx.send(state));
        };

        auto onCmd = [&](auto cmd) {
            using T = std::decay_t<decltype(cmd)>;
            if constexpr (std::is_same_v<T, SetFrequency>) onSetFreq(cmd);
            if constexpr (std::is_same_v<T, SetThreshold>) onSetThreshold(cmd);
            if constexpr (std::is_same_v<T, PiezoMode>) onSetMode(cmd);
        };

        auto onMsg = [&](auto item) {
            using T = std::decay_t<decltype(item)>;
            if constexpr (std::is_same_v<T, Measurement>) onMeasurement(item);
            if constexpr (std::is_same_v<T, Cmd>) std::visit(onCmd, item);
        };

        // assert(state_tx.send(state));

        while (true) {
            if (rx.receive(item)) {
                log_i("Piezo task received an item");
                std::visit(onMsg, item);
            }
        }
    }
};
