#pragma once

#include "https_request.h"
#include "https_response.h"
#include <esp32-hal-log.h>
#include <esp_https_server.h>
#include <forward_list>
#include <functional>
#include <string_view>

class HttpsServer {
    httpd_handle_t handle;
    std::string key;
    std::string cacert;

    using ReqHandler = std::function<void(HttpsRequest&, HttpsResponse&)>;

    struct Resource {
        ReqHandler handler;
        std::string uri;
    };

    std::forward_list<Resource> resources;

    HttpsServer(const HttpsServer&) = delete;
    HttpsServer(HttpsServer&&) = delete;

public:
    HttpsServer(std::string key, std::string cacert)
        : key(key)
        , cacert(cacert) { }

    bool start() {
        httpd_ssl_config_t conf = HTTPD_SSL_CONFIG_DEFAULT();
        // mbedtls expects the `len` to include null terminator
        conf.cacert_pem = (const uint8_t*)cacert.c_str();
        conf.cacert_len = cacert.size() + 1;
        conf.prvtkey_pem = (const uint8_t*)key.c_str();
        conf.prvtkey_len = key.size() + 1;
        conf.httpd.uri_match_fn = httpd_uri_match_wildcard;
        return httpd_ssl_start(&handle, &conf) == ESP_OK;
    }

    void stop() { httpd_ssl_stop(handle); }

    bool registerResource(std::string uri, httpd_method_t method, ReqHandler handler) {
        log_i("Registering resource [%s]", uri.c_str());
        resources.push_front(Resource { handler, uri });
        auto& resource = resources.front();
        httpd_uri_t node = {
            .uri = resource.uri.c_str(),
            .method = method,
            .handler = [](auto* request) {
                const auto& resource = *reinterpret_cast<const Resource*>(request->user_ctx);
                log_i("Built-in handler started [%s]", resource.uri.c_str());
                HttpsRequest req { request };
                HttpsResponse res { request };
                log_i("Executing user defined handler [%s]", req.getPath().data());
                resource.handler(req, res);
                log_i("Built-in handler finished");
                return ESP_OK;
            },
            .user_ctx = &resource,
        };
        return httpd_register_uri_handler(handle, &node) == ESP_OK;
    }
};