#pragma once

#include <freertos/FreeRTOS.h>

template <typename T>
class Mutex;

template <typename T>
class ScopedLock {
    SemaphoreHandle_t handle;
    T& data;

    ScopedLock(SemaphoreHandle_t handle, T& data)
        : handle(handle)
        , data(data) {
        assert(xSemaphoreTake(handle, portMAX_DELAY) == pdTRUE);
    }

    friend class Mutex<T>;

public:
    T& operator*() { return data; }

    ~ScopedLock() { assert(xSemaphoreGive(handle) == pdTRUE); }
};

template <typename T>
class Mutex {
    SemaphoreHandle_t handle;
    T data;

public:
    Mutex()
        : handle(xSemaphoreCreateMutex()) { assert(handle != NULL); }

    ScopedLock<T> lock() { return { handle, data }; }
};
