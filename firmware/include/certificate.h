#pragma once

#include <cstring>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/entropy.h>
#include <mbedtls/pk.h>
#include <mbedtls/x509_crt.h>
#include <string>
#include <utility>

class Entropy {
    mbedtls_entropy_context ctx;

    friend class Rng;

public:
    Entropy() { mbedtls_entropy_init(&ctx); }
    ~Entropy() { mbedtls_entropy_free(&ctx); }
};

class Rng {
    mbedtls_ctr_drbg_context ctx;

    friend class PrivateKey;
    friend class CertificateWriter;

public:
    Rng(Entropy& entropy) {
        mbedtls_ctr_drbg_init(&ctx);
        assert(0 == mbedtls_ctr_drbg_seed(&ctx, mbedtls_entropy_func, &entropy.ctx, nullptr, 0));
    }

    ~Rng() { mbedtls_ctr_drbg_free(&ctx); }
};

class PrivateKey {
    mbedtls_pk_context ctx;
    size_t keySize;

    friend class CertificateWriter;

public:
    PrivateKey(Rng& rng, size_t keySize = 2048)
        : keySize(keySize) {
        mbedtls_pk_init(&ctx);
        assert(0 == mbedtls_pk_setup(&ctx, mbedtls_pk_info_from_type(MBEDTLS_PK_RSA)));
        assert(0 == mbedtls_rsa_gen_key(mbedtls_pk_rsa(ctx), mbedtls_ctr_drbg_random, &rng.ctx, keySize, 65537));
    }

    static PrivateKey generate() {
        Entropy entropy;
        Rng rng(entropy);
        return PrivateKey(rng);
    }

    ~PrivateKey() { mbedtls_pk_free(&ctx); }

    std::string toPEM() {
        std::string res;
        res.resize(2 * keySize);
        assert(0 == mbedtls_pk_write_key_pem(&ctx, (unsigned char*)res.data(), res.size()));
        auto len = strlen(res.c_str());
        res.resize(len);
        res.shrink_to_fit();
        return res;
    }
};

class MPI {
    mbedtls_mpi ctx;

    friend class CertificateWriter;

public:
    MPI(std::string_view data) {
        mbedtls_mpi_init(&ctx);
        assert(0 == mbedtls_mpi_read_string(&ctx, 10, data.data()));
    }
    ~MPI() { mbedtls_mpi_free(&ctx); }
};

class CertificateWriter {
    mbedtls_x509write_cert ctx;

public:
    CertificateWriter() {
        mbedtls_x509write_crt_init(&ctx);
        mbedtls_x509write_crt_set_version(&ctx, MBEDTLS_X509_CRT_VERSION_3);
        mbedtls_x509write_crt_set_md_alg(&ctx, MBEDTLS_MD_SHA256);
    }
    ~CertificateWriter() { mbedtls_x509write_crt_free(&ctx); }

    void setSubjectKey(PrivateKey& key) { mbedtls_x509write_crt_set_subject_key(&ctx, &key.ctx); }
    void setIssuerKey(PrivateKey& key) { mbedtls_x509write_crt_set_issuer_key(&ctx, &key.ctx); }
    void setSubjectName(std::string_view name) { assert(0 == mbedtls_x509write_crt_set_subject_name(&ctx, name.data())); }
    void setIssuerName(std::string_view name) { assert(0 == mbedtls_x509write_crt_set_issuer_name(&ctx, name.data())); }
    void setValidity(std::string_view from, std::string_view to) { assert(0 == mbedtls_x509write_crt_set_validity(&ctx, from.data(), to.data())); }
    void setBasicConstraints(bool isCa, int maxPathLen) { assert(0 == mbedtls_x509write_crt_set_basic_constraints(&ctx, isCa, maxPathLen)); }
    void setSerial(MPI& serial) { assert(0 == mbedtls_x509write_crt_set_serial(&ctx, &serial.ctx)); }
    std::string toPEM(Rng& rng) {
        std::string res;
        res.resize(4096);
        assert(0 == mbedtls_x509write_crt_pem(&ctx, (unsigned char*)res.data(), res.size(), mbedtls_ctr_drbg_random, &rng.ctx));
        auto len = strlen(res.c_str());
        res.resize(len);
        res.shrink_to_fit();
        return res;
    }
};

class SelfSignedCertificate {
    std::string pem;

public:
    SelfSignedCertificate(PrivateKey& pk, std::string_view dn, std::string_view validFrom, std::string_view validTo) {
        CertificateWriter writer;
        writer.setSubjectKey(pk);
        writer.setIssuerKey(pk);
        writer.setSubjectName(dn);
        writer.setIssuerName(dn);
        writer.setValidity(validFrom, validTo);
        writer.setBasicConstraints(true, 0);
        MPI serial("1");
        writer.setSerial(serial);
        Entropy entropy;
        Rng rng(entropy);
        pem = writer.toPEM(rng);
    }

    std::string toPEM() { return pem; }
};

std::pair<std::string, std::string> make_self_signed_pem(std::string_view dn, std::string_view validFrom, std::string_view validTo) {
    auto pk = PrivateKey::generate();
    SelfSignedCertificate cacert(pk, dn, validFrom, validTo);
    return std::make_pair(pk.toPEM(), cacert.toPEM());
}