#pragma once

#include <esp_https_server.h>
#include <optional>
#include <string_view>

class HttpsRequest {
    httpd_req_t* req;

    friend class HttpsServer;

    HttpsRequest(httpd_req_t* req)
        : req(req) { }

public:
    std::string_view getPath() const { return req->uri; }

    std::optional<std::string> getQueryParameter(std::string_view key) const {
        auto len = httpd_req_get_url_query_len(req);
        if (len == 0) return {};
        std::string query;
        query.resize(len + 1);
        assert(httpd_req_get_url_query_str(req, query.data(), query.size()) == ESP_OK);
        std::string value;
        value.resize(256);
        assert(httpd_query_key_value(query.c_str(), key.data(), value.data(), value.size()) == ESP_OK);
        value.resize(strlen(value.c_str()));
        value.shrink_to_fit();
        return value;
    }
};