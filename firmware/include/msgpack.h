#pragma once

#include <map>
#include <span>
#include <string_view>
#include <utility>
#include <vector>

namespace msgpack {

using bytes = std::span<const uint8_t>;

uint16_t u16_from_be(bytes d) { return (d[0] << 8) | d[1]; }
uint32_t u32_from_be(bytes d) { return (d[0] << 24) | (d[1] << 16) | (d[2] << 8) | d[3]; }

template <typename T>
using ParseResult = std::optional<std::pair<T, bytes>>;

template <typename T>
struct tag { };

template <typename T>
ParseResult<T> parse(bytes b) { return parse(b, tag<T> {}); }

ParseResult<bool> parse(bytes b, tag<bool>) {
    if (b[0] == 0xc2) return std::make_pair(false, b.subspan(1));
    if (b[0] == 0xc3) return std::make_pair(true, b.subspan(1));
    log_e("Wrong bool format");
    return {};
}

struct FixInt {
    int8_t value;
    operator int8_t() const { return value; }
};

ParseResult<FixInt> parse(bytes b, tag<FixInt>) {
    if ((b[0] >> 7) == 0) return std::make_pair(FixInt { (int8_t)b[0] }, b.subspan(1));
    if ((b[0] >> 5) == 0b111) return std::make_pair(FixInt { (int8_t)b[0] }, b.subspan(1));
    log_e("Wrong fixint format");
    return {};
}

ParseResult<uint8_t> parse(bytes b, tag<uint8_t>) {
    if (b[0] == 0xcc) return std::make_pair(b[1], b.subspan(2));
    log_e("Wrong uint8 format");
    return {};
}

ParseResult<float> parse(bytes b, tag<float>) {
    if (b[0] == 0xca) {
        float f;
        std::array<uint8_t, 4> buf;
        std::memcpy(buf.begin(), &b[1], 4);
        // convert big endian to little endian
        std::swap(buf[0], buf[3]);
        std::swap(buf[1], buf[2]);
        std::memcpy(&f, buf.cbegin(), 4);
        return std::make_pair(f, b.subspan(5));
    }
    log_e("Wrong float32 format");
    return {};
}

template <typename T, typename... Ts>
ParseResult<std::variant<T, Ts...>> parse(bytes b, tag<std::variant<T, Ts...>>) {
    auto res = parse<T>(b);
    if (res) return *res;
    if constexpr (sizeof...(Ts) > 0) {
        auto res = parse<std::variant<Ts...>>(b);
        if (res) return std::make_pair(std::visit([](auto& v) -> std::variant<T, Ts...> { return v; }, res->first), res->second);
    }
    return {};
}

ParseResult<std::string_view> parse(bytes b, tag<std::string_view>) {
    if ((b[0] >> 5) == 0x05) {
        size_t len = b[0] & 0x1f;
        return std::make_pair(std::string_view { reinterpret_cast<const char*>(&b[1]), len }, b.subspan(1 + len));
    }
    if (b[0] == 0xd9) {
        auto len = b[1];
        return std::make_pair(std::string_view { reinterpret_cast<const char*>(&b[2]), len }, b.subspan(2 + len));
    }
    if (b[0] == 0xda) {
        auto len = u16_from_be(b.subspan(1));
        return std::make_pair(std::string_view { reinterpret_cast<const char*>(&b[3]), len }, b.subspan(3 + len));
    }
    if (b[0] == 0xdb) {
        auto len = u32_from_be(b.subspan(1));
        return std::make_pair(std::string_view { reinterpret_cast<const char*>(&b[5]), len }, b.subspan(5 + len));
    }
    log_e("Wrong str format");
    return {};
}

ParseResult<bytes> parse(bytes b, tag<bytes>) {
    if (b[0] == 0xc4) {
        auto len = b[1];
        return std::make_pair(b.subspan(2, len), b.subspan(2 + len));
    }
    if (b[0] == 0xc5) {
        auto len = u16_from_be(b.subspan(1));
        return std::make_pair(b.subspan(3, len), b.subspan(3 + len));
    }
    if (b[0] == 0xc6) {
        auto len = u32_from_be(b.subspan(1));
        return std::make_pair(b.subspan(5, len), b.subspan(5 + len));
    }
    log_e("Wrong bin format");
    return {};
}

template <typename K, typename V>
ParseResult<std::map<K, V>> parse(bytes b, tag<std::map<K, V>>) {

    auto parse_entries = [](size_t n, bytes b) -> ParseResult<std::map<K, V>> {
        std::map<K, V> m;

        while (n--) {
            auto [k, br] = parse<K>(b).value();
            auto [v, brr] = parse<V>(br).value();
            b = brr;
            log_i("found entry");
            m.insert_or_assign(k, v);
        }

        return std::make_pair(m, b);
    };

    if ((b[0] >> 4) == 0x08) {
        size_t len = b[0] & 0x0f;
        return parse_entries(len, b.subspan(1));
    } else if (b[0] == 0xde) {
        auto len = u16_from_be(b.subspan(1));
        return parse_entries(len, b.subspan(3));
    } else if (b[0] == 0xdf) {
        auto len = u32_from_be(b.subspan(1));
        return parse_entries(len, b.subspan(5));
    }
    log_e("Wrong map format");
    return {};
}

template <typename T, typename... Ts>
ParseResult<std::tuple<T, Ts...>> parse(bytes b, tag<std::tuple<T, Ts...>>) {
    auto res = parse<T>(b);
    if (!res) return {};
    auto [val, b2] = res.value();
    auto tuple = std::tuple<T> { val };
    if constexpr (sizeof...(Ts) == 0) {
        return std::make_pair(tuple, b2);
    } else {
        auto res2 = parse<std::tuple<Ts...>>(b2);
        if (!res2) return {};
        auto [val2, b3] = res2.value();
        return std::make_pair(std::tuple_cat(tuple, val2), b3);
    }
}

using bytevec = std::vector<uint8_t>;

template <typename T>
bytevec serialize(T const& data) {
    bytevec res {};
    // We need to use ADL in order to lookup user defined function overloads.
    serialize(data, res, tag<T> {});
    return res;
}

void serialize(bool data, bytevec& res, tag<bool>) {
    res.push_back(data ? 0xc3 : 0xc2);
}

void serialize(uint8_t data, bytevec& res, tag<uint8_t>) {
    res.push_back(0xcc);
    res.push_back(data);
}

void serialize(float data, bytevec& res, tag<float>) {
    res.push_back(0xca);
    std::array<uint8_t, 4> buf;
    std::memcpy(buf.begin(), &data, 4);
    // convert little endian to big endian
    std::swap(buf[0], buf[3]);
    std::swap(buf[1], buf[2]);
    std::copy(buf.cbegin(), buf.cend(), std::back_inserter(res));
}

void serialize(std::string_view data, bytevec& res, tag<std::string_view>) {
    assert(data.size() < 32);
    uint8_t len = data.size();
    res.push_back(0xa0 | len);
    std::copy(data.begin(), data.end(), std::back_inserter(res));
}

template <typename... Ts>
void serialize(std::variant<Ts...> const& data, bytevec& res, tag<std::variant<Ts...>>) {
    // clang-format off
    std::visit([&](auto const& data) {
        serialize(data, res, tag<std::decay_t<decltype(data)>> {});
    }, data);
    // clang-format on
}

template <typename K, typename V>
void serialize(std::map<K, V> const& data, bytevec& res, tag<std::map<K, V>>) {
    assert(data.size() < 16);
    uint8_t len = data.size();
    res.push_back(0x80 | len);
    for (auto const& entry : data) {
        serialize(entry.first, res, tag<K> {});
        serialize(entry.second, res, tag<V> {});
    }
}

template <typename... Ts>
void serialize(std::tuple<Ts...> const& data, bytevec& res, tag<std::tuple<Ts...>>) {
    // clang-format off
    std::apply([&](auto const&... vals) {
        (serialize(vals, res, tag<std::decay_t<decltype(vals)>> {}), ...);
    }, data);
    // clang-format on
}

}