#pragma once

#include <vector>

template <typename T>
class Window {
    int _begin { 0 };
    int _end { 0 };
    std::vector<T> data;

    struct _Iter {
        Window<T> const& vec;
        int index;

        void operator++() {
            index = (index + 1) % vec.data.size();
        }

        bool operator==(_Iter const& other) {
            return other.index == index;
        }
    };

public:
    Window(int window_len) {
        data.resize(window_len);
    }

    void push(const T& item) {
        data[_begin++] = item;
        _begin %= data.size();
        if (_begin == _end)
            _end = (_end + 1) % data.size();
    }

    auto begin() const {
        return _Iter { .vec = *this, .index = _end };
    }

    auto end() const {
        return _Iter { .vec = *this, .index = _begin };
    }
};
