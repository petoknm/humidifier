#pragma once

#include <cstddef>
#include <esp_https_server.h>
#include <span>
#include <string_view>

class HttpsResponse {
    httpd_req_t* req;

    friend class HttpsServer;

    HttpsResponse(httpd_req_t* req)
        : req(req) { }

public:
    void setStatus(std::string_view status) {
        assert(httpd_resp_set_status(req, status.data()) == ESP_OK);
    }

    void setContentType(std::string_view value) {
        assert(httpd_resp_set_type(req, value.data()) == ESP_OK);
    }

    void setHeader(std::string_view field, std::string_view value) {
        if (field == "Content-Type") setContentType(value);
        else assert(httpd_resp_set_hdr(req, field.data(), value.data()) == ESP_OK);
    }

    void write(std::string_view str) {
        assert(httpd_resp_send(req, str.data(), str.length()) == ESP_OK);
    }

    void write(const std::byte* buf, size_t len) {
        assert(httpd_resp_send(req, reinterpret_cast<const char*>(buf), len) == ESP_OK);
    }

    void write(std::span<const std::byte> data) { write(data.data(), data.size()); }

    void write_chunk(std::string_view str) {
        assert(httpd_resp_send_chunk(req, str.data(), str.length()) == ESP_OK);
    }

    void write_chunk(const std::byte* buf, size_t len) {
        assert(httpd_resp_send_chunk(req, reinterpret_cast<const char*>(buf), len) == ESP_OK);
    }

    void write_chunk(std::span<const std::byte> data) { write(data.data(), data.size()); }
};
