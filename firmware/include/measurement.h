#pragma once

template <typename T, size_t FractionalDigits, T ZeroPoint = 0>
struct Decimal {
    using _ = std::enable_if_t<std::is_unsigned_v<T>>();

    T data;

    Decimal operator+(const Decimal& other) const { return { data + other.data }; }
    Decimal operator-(const Decimal& other) const { return { data - other.data }; }
    Decimal operator*(size_t divisor) const { return { data * divisor }; }
    Decimal operator/(size_t divisor) const { return { data / divisor }; }
};

Decimal<uint16_t, 2> a;

struct Measurement {
    float temp;
    float humi;
};