#pragma once

#include "channel.h"
#include "measurement.h"
#include <AHT10.h>
#include <esp32-hal-log.h>
#include <vector>

class SensorTask {
    using Tx = OwningSender<Measurement>;
    std::vector<Tx> senders;

    static constexpr int priority = 10;

public:
    template <typename... Txs>
    SensorTask(Txs... txs)
        : senders({ txs... }) { }

    [[nodiscard]] bool start() {
        auto res = xTaskCreatePinnedToCore(
            [](void* arg) { reinterpret_cast<SensorTask*>(arg)->task(); },
            "sensord", 1024, this, priority, nullptr, 1);
        return res == pdPASS;
    }

private:
    void send(Measurement m) {
        log_i("Sending new measurement");
        for (auto sender : senders) {
            assert(sender.send(m));
        }
    }

    void task() {
#if 0
        AHT10 sensor(AHT10_ADDRESS_0X38);

        int counter = 0;

        while (true) {
            sensor.readRawData();
            auto temp = sensor.readTemperature(false);
            auto humi = sensor.readHumidity(false);

            mutex->lock();

            hd_temp.push(temp);
            hd_humi.push(humi);

            counter++;
            if (counter >= 60) {
                counter = 0;

                sd_temp.push(temp);
                sd_humi.push(humi);
            }

            mutex->release();

            delay(5000);
        }
#else
        delay(20000);
        float radians = 0;
        while (true) {
            auto val = 20 + 5 * sin(radians);
            send({ .temp = val, .humi = val });
            radians += 0.1;
            delay(5000);
        }
#endif
    }
};
