#pragma once

#include "channel.h"
#include "queue.h"
#include <freertos/queue.h>
#include <tuple>
#include <variant>

struct ToQueue {
    template <typename T>
    static Queue<T>& convert(OwningReceiver<T>& rx) { return *rx.queue; }
    template <typename T>
    static Queue<T>& convert(Queue<T>& queue) { return queue; }
};

template <typename... Ts>
class MultiReceiver {
    QueueSetHandle_t handle;
    std::tuple<Queue<Ts>&...> queues;

    template <typename Qs, typename I, size_t N>
    [[gnu::always_inline]] static bool helper(QueueSetMemberHandle_t ready_q, Qs& queues, I& item) {
        auto& q = std::get<N>(queues);
        using X = typename std::decay_t<decltype(q)>::Item;
        if (q.handle == ready_q) {
            X x;
            assert(q.receive(x, 0));
            item = x;
            return true;
        }
        if constexpr (N == 0) return false;
        else return helper<Qs, I, N - 1>(ready_q, queues, item);
    }

public:
    using Item = std::variant<Ts...>;

    template <typename... Rxs>
    MultiReceiver(int len, Rxs&... rxs)
        : queues(ToQueue::convert(rxs)...) {
        handle = xQueueCreateSet(len);
        auto add = [&](auto&... queues) { (assert(xQueueAddToSet(queues.handle, handle) == pdPASS), ...); };
        std::apply(add, queues);
    }

    [[nodiscard]] bool receive(Item& item, TickType_t wait = portMAX_DELAY) {
        auto ready_q = xQueueSelectFromSet(handle, wait);
        if (ready_q == nullptr) return false;
        return helper<std::tuple<Queue<Ts>&...>&, Item&, sizeof...(Ts) - 1>(ready_q, queues, item);
    }

    ~MultiReceiver() {
        (assert(xQueueRemoveFromSet(std::get<Queue<Ts>&>(queues).handle, handle) == pdPASS), ...);
        vQueueDelete(handle);
    }
};

template <typename... Rxs>
MultiReceiver(int, Rxs&... rxs) -> MultiReceiver<typename Rxs::Item...>;

template <typename... Rxs>
MultiReceiver<typename Rxs::Item...> make_multi_receiver(int len, Rxs... rxs) { return { len, rxs... }; }
