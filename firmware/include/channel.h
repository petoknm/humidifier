#pragma once

#include "queue.h"
#include <memory>

template <typename T>
class OwningSender {
    template <typename U>
    friend auto make_channel(size_t len);

    std::shared_ptr<Queue<T>> queue;

    OwningSender(std::shared_ptr<Queue<T>> queue)
        : queue(queue) { }

public:
    [[nodiscard]] bool send(const T& item, TickType_t wait = portMAX_DELAY) {
        return queue->send(item, wait);
    }
};

template <typename T>
class OwningReceiver {
    template <typename U>
    friend auto make_channel(size_t len);
    friend struct ToQueue;

    std::shared_ptr<Queue<T>> queue;

    OwningReceiver(std::shared_ptr<Queue<T>> queue)
        : queue(queue) { }

public:
    using Item = T;

    [[nodiscard]] bool receive(T& item, TickType_t wait = portMAX_DELAY) const {
        return queue->receive(item, wait);
    }
};

template <typename T>
auto make_channel(size_t len) {
    auto queue = std::make_shared<Queue<T>>(len);
    return std::make_pair(OwningSender(queue), OwningReceiver(queue));
}
