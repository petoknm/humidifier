#pragma once

#include <SPIFFS.h>
#include <esp32-hal-log.h>
#include <freertos/FreeRTOS.h>
#include <span>
#include <utility>
#include <variant>

#include "certificate.h"
#include "channel.h"
#include "https_server.h"
#include "measurement.h"
#include "msgpack.h"
#include "multi_receiver.h"
#include "mutex.h"
#include "piezo_task.h"

namespace msgpack {
using piezo_state_tuple = std::tuple<uint8_t, float, float, float, bool>;
ParseResult<PiezoState> parse(bytes b, tag<PiezoState>) {
    return parse<piezo_state_tuple>(b).transform([](auto res) {
        auto [tuple, b] = res;
        auto [mode, humi_lo, humi_hi, freq, running] = tuple;
        auto state = PiezoState { PiezoMode { mode }, humi_lo, humi_hi, freq, running };
        return std::make_pair(state, b);
    });
}
void serialize(PiezoState const& data, bytevec& res, tag<PiezoState>) {
    auto tuple = piezo_state_tuple { static_cast<uint8_t>(data.mode), data.humi_lo, data.humi_hi, data.freq, data.running };
    serialize(tuple, res, tag<piezo_state_tuple> {});
}
}

constexpr std::string_view mimeTypes[][2] = {
    { ".html", "text/html" },
    { ".css", "text/css" },
    { ".js", "application/javascript" },
};

std::string_view contentType(std::string_view filename) {
    for (auto pair : mimeTypes) {
        if (filename.ends_with(pair[0])) return pair[1];
    }
    return "text/html";
}

extern const uint8_t public_data_start asm("_binary_public_msgpack_start");
extern const uint8_t public_data_end asm("_binary_public_msgpack_end");

class ServerTask {
    OwningReceiver<Measurement> measurement_rx;
    OwningReceiver<PiezoState> piezo_rx;
    PiezoTask& piezo_task;

    static constexpr int priority = 10;

public:
    ServerTask(OwningReceiver<Measurement> measurement_rx, OwningReceiver<PiezoState> piezo_rx, PiezoTask& piezo_task)
        : measurement_rx(measurement_rx)
        , piezo_rx(piezo_rx)
        , piezo_task(piezo_task) { }

    [[nodiscard]] bool start() {
        auto res = xTaskCreatePinnedToCore(
            [](void* arg) { reinterpret_cast<ServerTask*>(arg)->task(); },
            "httpsd", 8 * 1024, this, priority, nullptr, 1);
        return res == pdPASS;
    }

private:
    static constexpr std::string_view KEY_FILENAME = "/key.pem";
    static constexpr std::string_view CRT_FILENAME = "/cacert.pem";

    static void save_file(std::string_view path, std::string_view data) {
        auto file = SPIFFS.open(path.data(), "w", true);
        file.write((const uint8_t*)data.data(), data.size());
        file.close();
    }

    static std::string load_file(std::string_view path) {
        auto file = SPIFFS.open(path.data());
        auto str = file.readString();
        file.close();
        return str.c_str();
    }

    static auto get_cert() {
        std::string key, cacert;
        if (SPIFFS.exists(KEY_FILENAME.data()) && SPIFFS.exists(CRT_FILENAME.data())) {
            log_i("Loading certificate from SPIFFS...");
            key = load_file(KEY_FILENAME);
            cacert = load_file(CRT_FILENAME);
        } else {
            log_i("Creating a self signed certificate...");
            auto res = make_self_signed_pem("CN=humi.local,O=FancyCompany,C=DK",
                                            "20220101000000", "20320101000000");
            key = res.first;
            cacert = res.second;
            save_file(KEY_FILENAME, key);
            save_file(CRT_FILENAME, cacert);
        }
        return std::make_pair(key, cacert);
    }

    void task() {
        Mutex<Measurement> last_measurement;
        Mutex<PiezoState> piezo_state;

        if (!SPIFFS.begin(true)) {
            log_i("SPIFFS failed to initialize");
            while (true) delay(1000);
        }

        auto public_data = msgpack::bytes(&public_data_start, &public_data_end);
        auto public_map = msgpack::parse<std::map<std::string_view, msgpack::bytes>>(public_data).value().first;

        auto [key, cacert] = get_cert();
        log_i("PK = \n%s", key.c_str());
        log_i("CACERT = \n%s", cacert.c_str());

        HttpsServer server(key, cacert);

        log_i("Starting HTTPS server...");
        if (!server.start()) {
            log_i("Server could not start");
            while (true) delay(1000);
        }
        log_i("Server listening");
        log_i("Registering handlers");

        server.registerResource("/api/sensor", HTTP_GET, [&](auto& req, auto& res) {
            auto format = req.getQueryParameter("format").value_or("sd");

            std::vector<uint16_t> data(16, 0xABBA);

            // TODO
            if (format == "sd") {
                // on_sensor_sd(data);
            } else if (format == "hd") {
                // on_sensor_hd(data);
            } else {
                res.setStatus("400 Bad request");
                return;
            }

            res.setHeader("Content-Type", "application/octet-stream");
            res.write(std::as_bytes(std::span { data }));
        });

        server.registerResource("/api/piezo", HTTP_GET, [&](auto& req, auto& res) {
            res.setHeader("Content-Type", "application/msgpack");
            auto data = msgpack::serialize(*piezo_state.lock());
            res.write(std::as_bytes(std::span { data }));
        });

        struct NotFound { };
        struct Compressed {
            msgpack::bytes data;
        };
        struct Uncompressed {
            msgpack::bytes data;
        };

        using LoadResult = std::variant<NotFound, Compressed, Uncompressed>;

        auto load_static_file = [&](std::string_view path) -> LoadResult {
            auto path_gz = std::string(path) + ".gz";
            auto value = public_map.find(path_gz);
            if (value != public_map.end()) return Compressed { value->second };
            value = public_map.find(path);
            if (value != public_map.end()) return Uncompressed { value->second };
            return NotFound {};
        };

        server.registerResource("/*", HTTP_GET, [&](auto& req, auto& res) {
            auto path = req.getPath();
            path = path == "/" ? "/index.html" : path;
            auto result = load_static_file(path);

            if (std::holds_alternative<NotFound>(result)) {
                res.setStatus("404 Not found");
                res.write("404 Not found");
            } else if (std::holds_alternative<Compressed>(result)) {
                auto data = std::get<Compressed>(result).data;
                res.setHeader("Content-Type", contentType(path));
                res.setHeader("Content-Encoding", "gzip");
                res.write(std::as_bytes(data));
            } else if (std::holds_alternative<Uncompressed>(result)) {
                auto data = std::get<Compressed>(result).data;
                res.setHeader("Content-Type", contentType(path));
                res.write(std::as_bytes(data));
            }
        });

        log_i("Server ready");

        auto rx = make_multi_receiver(16, piezo_rx, measurement_rx);
        decltype(rx)::Item item;

        auto onMeasurement = [&](Measurement m) {
            log_i("Got measurement");
            *last_measurement.lock() = m;
        };
        auto onPiezoState = [&](PiezoState state) {
            log_i("Got piezo state");
            *piezo_state.lock() = state;
        };

        auto onItem = [&](auto item) {
            using T = std::decay_t<decltype(item)>;
            if constexpr (std::is_same_v<T, Measurement>) onMeasurement(item);
            if constexpr (std::is_same_v<T, PiezoState>) onPiezoState(item);
        };

        while (true) {
            if (rx.receive(item)) {
                log_i("Server task received msg");
                std::visit(onItem, item);
            }
        }
    }
};
