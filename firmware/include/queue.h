#pragma once

#include <esp32-hal-log.h>
#include <freertos/FreeRTOS.h>

template <typename T>
class Queue {
    template <typename... Ts>
    friend class MultiReceiver;

    QueueHandle_t handle;

public:
    using Item = T;

    Queue(int len) {
        handle = xQueueCreate(len, sizeof(T));
        log_i("Task %s created queue %p", pcTaskGetName(nullptr), handle);
    }

    [[nodiscard]] bool send(const T& item, TickType_t wait = portMAX_DELAY) {
        auto res = xQueueSend(handle, &item, wait);
        return res == pdTRUE;
    }

    [[nodiscard]] bool receive(T& item, TickType_t wait = portMAX_DELAY) const {
        auto res = xQueueReceive(handle, &item, wait);
        return res == pdTRUE;
    }

    ~Queue() {
        log_i("Task %s deleted queue %p", pcTaskGetName(nullptr), handle);
        vQueueDelete(handle);
    }
};