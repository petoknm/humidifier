import { Component } from "solid-js";

interface SectionProps { title: string }

export const Section: Component<SectionProps> = props => {
    return <div class="w-full h-full flex flex-col">
        <h2 class="text-2xl text-white px-6">{props.title}</h2>
        <div class="grow p-4">{props.children}</div>
    </div>;
};