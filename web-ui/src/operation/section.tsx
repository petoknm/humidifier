import { Component, createMemo, createSignal } from "solid-js";
import { Section } from "../section";
import { OperatingMode } from "./api";


export const OperationSection: Component = () => {
    const [mode, setMode] = createSignal(OperatingMode.Auto);

    return <Section title="Operation">
        <div class="h-full flex flex-col-3 gap-4 text-white">
            <ModeCard isActive={mode() == OperatingMode.Off} onClick={() => setMode(OperatingMode.Off)}>
                <i class="fi fi-rr-bulb text-2xl text-center"></i>
                <h3>Off</h3>
            </ModeCard>
            <ModeCard isActive={mode() == OperatingMode.Auto} onClick={() => setMode(OperatingMode.Auto)}>
                <i class="fi fi-rr-magic-wand text-2xl text-center"></i>
                <h3>Auto</h3>
            </ModeCard>
            <ModeCard isActive={mode() == OperatingMode.On} onClick={() => setMode(OperatingMode.On)}>
                <i class="fi fi-sr-bulb text-2xl text-center"></i>
                <h3>On</h3>
            </ModeCard>
        </div>
    </Section>;
};

interface ModeCardProps {
    isActive: boolean;
    onClick: () => void;
}

const ModeCard: Component<ModeCardProps> = (props) => {
    return <div
        class="basis-1/3 rounded-2xl drop-shadow-xl p-4 flex flex-col items-center justify-center"
        classList={{ 'bg-cyan-grey-500': props.isActive, 'bg-blue-700': !props.isActive }}
        onclick={props.onClick}>
        {props.children}
    </div>;
};