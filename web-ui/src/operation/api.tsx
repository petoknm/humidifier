import { decode, encode } from "messagepack";

export enum OperatingMode { Auto, On, Off }

export interface DeviceState {
    humi_lo: number;
    humi_hi: number;
    freq: number;
    mode: OperatingMode;
    running: boolean;
}

export const fetchDeviceState: () => Promise<DeviceState> = async () => {
    if (import.meta.env.DEV) {
        await new Promise(resolve => setTimeout(resolve, 1000));
        return {
            humi_lo: 50,
            humi_hi: 60,
            freq: 108000,
            mode: OperatingMode.Auto,
            running: true,
        };
    }
    const res = await fetch("/api/piezo");
    const body = await res.arrayBuffer();
    // return {} as any;
    return msgpack.decode(body);
};