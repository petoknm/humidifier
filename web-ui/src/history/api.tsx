export enum Format { SD, HD }

export const fetchHumidity: (Format) => Promise<number[]> = async (format: Format) => {
    if (import.meta.env.DEV) {
        await new Promise(resolve => setTimeout(resolve, 1000));
        return [1000, 2000, 3000, 4000];
    }
    const res = await fetch("/api/sensor");
    const data = await res.arrayBuffer();
    return new Int16Array(data);
}