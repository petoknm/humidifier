import { Component, onMount } from "solid-js";

export const HumidityCard: Component = () => {
    return <div class="w-full h-full rounded-2xl bg-blue-700 drop-shadow-xl grid grid-cols-1">
        <div style="grid-area: 1/1/1/1">
            <div class="p-4 flex flex-row justify-between text-white">
                <i class="fi fi-rr-raindrops text-xl"></i>
                <div><span class="text-xl">62 </span><span class="text-sm">%RH</span></div>
            </div>
        </div>
        <div style="grid-area: 1/1/1/1">
            <Sparkline points={[10, 20, 15, 30, 25]} ymin={0} ymax={40}></Sparkline>
        </div>
    </div>;
};

export const TemperatureCard: Component = () => {
    return <div class="w-full h-full rounded-2xl bg-blue-700 drop-shadow-xl grid grid-cols-1">
        <div style="grid-area: 1/1/1/1">
            <div class="p-4 flex flex-row justify-between text-white">
                <i class="fi fi-rr-thermometer-half text-xl"></i>
                <div><span class="text-xl">24 </span><span class="text-sm">°C</span></div>
            </div>
        </div>
        <div style="grid-area: 1/1/1/1">
            <Sparkline points={[35, 20, 15, 30, 25]} ymin={10} ymax={40}></Sparkline>
        </div>
    </div>;
};


interface SparklineData { points: number[], ymin: number, ymax: number }

const Sparkline: Component<SparklineData> = ({ points, ymin, ymax }: SparklineData) => {
    let canvas;

    onMount(() => {
        canvas.height = canvas.clientHeight;
        const ctx = canvas.getContext('2d');
        // @ts-ignore
        const chart = new Chart(ctx, {
            type: 'lineWithShadow',
            data: {
                labels: ['1', '2', '3', '4', '5'],
                datasets: [{
                    data: points,
                    lineTension: 1 / 3,
                    pointRadius: 0,
                    borderWidth: 1.5,
                    borderColor: '#fff6',
                }],
            },
            options: {
                plugins: {
                    legend: { display: false },
                },
                scales: {
                    // @ts-ignore
                    xAxis: { display: false, },
                    yAxis: { display: false, min: ymin, max: ymax },
                },
            }
        });
    });

    return <canvas ref={canvas} class="w-full h-full"></canvas>;
};
