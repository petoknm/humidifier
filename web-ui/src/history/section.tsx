import { Component } from "solid-js";
import { Section } from "../section";
import { HumidityCard, TemperatureCard } from "./card";


export const HistorySection: Component = () => {
    return <Section title="History">
        <div class="h-full flex flex-row items-stretch gap-4">
            <div class="basis-3/5"><HumidityCard></HumidityCard></div>
            <div class="basis-2/5"><TemperatureCard></TemperatureCard></div>
        </div>
    </Section>;
};