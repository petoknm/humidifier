import { Component } from "solid-js";
import { Section } from "../section";


export const SettingsSection: Component = () => {
    return <Section title="Settings">
        <div class="h-full flex flex-row gap-4 text-white">
            <div class="basis-1/3 bg-blue-700 rounded-2xl drop-shadow-xl p-4 flex flex-col items-center justify-center">
                <i class="fi fi-rr-water"></i>
                <h3>Humidistat</h3>
            </div>
            <div class="basis-1/3 bg-blue-700 rounded-2xl drop-shadow-xl p-4 flex flex-col items-center justify-center">
                <i class="fi fi-rr-water"></i>
                <h3>Frequency</h3>
            </div>
        </div>
    </Section>;
};