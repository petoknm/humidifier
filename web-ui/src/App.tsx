import { Component, createResource, createSignal } from 'solid-js';
import { HistorySection } from './history/section';
import { OperationSection } from './operation/section';
import { SettingsSection } from './settings/section';

const App: Component = () => {
  return <div class="h-screen flex flex-col items-stretch bg-blue-900">
    <h1 class="text-white text-center text-2xl mx-6 my-4">~ Humidifier ~</h1>
    <div class="basis-1/5"><OperationSection></OperationSection></div>
    <div class="basis-1/4"><HistorySection></HistorySection></div>
    <div class="basis-1/5"><SettingsSection></SettingsSection></div>
  </div>;
};

export default App;
