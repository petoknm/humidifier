import './index.css';
import { render } from 'solid-js/web';

import App from './App';

// @ts-ignore
class lineWithShadow extends Chart.controllers.line {
    draw() {
        const ctx = this._ctx;
        ctx.shadowColor = 'white';
        ctx.shadowBlur = 10;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
        super.draw();
    }
}

lineWithShadow.id = 'lineWithShadow';
// @ts-ignore
Chart.register(lineWithShadow);

render(() => <App />, document.getElementById('root') as HTMLElement);
