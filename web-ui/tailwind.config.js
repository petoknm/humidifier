module.exports = {
  content: [
    './index.html',
    './src/**/*.{js,ts,jsx,tsx,css,md,mdx,html,json,scss}',
  ],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        // teal: {
        //   500: '#0DB2B2',
        // },
        // purple: {
        //   700: '#316890',
        //   900: '#5A166B',
        // },
        cyan: {
          300: '#0ad6de',
        },
        'cyan-grey': {
          500: '#8ca9a5',
        },
        blue: {
          900: '#282b4e',
          700: '#363868',
        }
      },
    },
  },
  plugins: [],
};
